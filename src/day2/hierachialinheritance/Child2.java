package day2.hierachialinheritance;

public class Child2 extends Parent{
	
	void property3() {
		System.out.println("Child2 Property3");
	}

	public static void main(String[] args) {
		
		Parent p=new Parent();
		p.property1();
		System.out.println();
		
		Child1 c1=new Child1();
		c1.property1();
		c1.property2();
		System.out.println();
		
		Child2 c2=new Child2();
		c2.property1();
		c2.property3();

	}

}
