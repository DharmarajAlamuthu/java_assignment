package day2.string;

public class CountAlphabet {

	public static void main(String[] args) {
		
		String str="Java Programming";
		int totalCount=str.length();
		int totalRemoveCount=str.replace("a", "").length();
		int count=totalCount-totalRemoveCount;
		
		System.out.println("Find occurances of a is: "+count);

	}

}
