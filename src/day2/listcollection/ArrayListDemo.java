package day2.listcollection;

import java.util.ArrayList;

public class ArrayListDemo {

	public static void main(String[] args) {
		
		ArrayList<String> al=new ArrayList<>();
		
		al.add("a");
		al.add("b");
		al.add("c");
		al.add("d");
		al.add("e");
		
		System.out.println(al.size());

	}

}
