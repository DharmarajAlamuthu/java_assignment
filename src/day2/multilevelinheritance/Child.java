package day2.multilevelinheritance;

public class Child extends Parent{
	
	void property3() {
		System.out.println("Child Property3");
	}

	public static void main(String[] args) {
		
		GrandParent gp=new GrandParent();
		gp.property1();
		
		Parent p=new Parent();
		p.property1();
		p.property2();
		
		Child c=new Child();
		c.property1();
		c.property2();
		c.property3();

	}

}
