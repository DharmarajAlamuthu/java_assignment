package day2.treeset;

import java.util.TreeSet;

public class Demo {

	public static void main(String[] args) {
		
		TreeSet<String> ts=new TreeSet<String>();
		ts.add("Dharma");
		ts.add("Kumarn");
		ts.add("Velavan");
		ts.add("Murugan");
		ts.add("Karthik");
		
		System.out.println(ts);

	}

}
