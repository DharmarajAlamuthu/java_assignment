package day2.hashmap;

import java.util.HashMap;

public class Demo {

	public static void main(String[] args) {
		
		HashMap<String, Integer> hm=new HashMap<String, Integer>();
		hm.put("a", 3);
		hm.put("b", 6);
		hm.put("c", 2);
		hm.put("d", 5);
		hm.put("e", 4);
		
		System.out.println(hm);
	}

}
