package com.pattern;

public class Pattern {

	public static void main(String[] args) {
		
		int num=5;
		for (int i = 1; i <=num; i++) {
			for (int j = i; j >=1; j--) {
				System.out.print(j+" ");
			}
			System.out.println();
		}

	}

}
//i=1	1<=5	j=1		1>=1	1
//i=2	2<=5	j=2		2>=1	2	j=2-1=1		1>=1	1
//i=3	3<=5	j=3		3>=1	3	j=3-1=2		2>=1	2	j=2-1=1		1
//i=4	4<=5	j=4		4>=1	4	j=4-1=3		3>=1	3	j=3-1=2		2	j=2-1=1		1
//i=5	5<=5	j=5		2>=1	5	j=5-1=4		4>=1	4	j=4-1=3		3	j=3-1=2		2	j=2-1=1	1