package com.pattern;

public class Pattern2 {

	public static void main(String[] args) {
		
		int alphabet=65;
		for (int i = 0; i <=3; i++) {
			for (int j = 0; j <=i; j++) {
				System.out.print((char)alphabet+" ");
			}
			alphabet++;
			System.out.println();
		}

	}

}
//alphabet=65	i=0		0<=3	j=0		0<=0	A	
//alphabet=66	i=1		1<=3	j=0		0<=0	B	j=0+1=1	1<=1	B
//alphabet=67	i=2		2<=3	j=0		0<=0	C	j=0+1=1		1<=2	C	j=1+1=2		2<=2	C
//alphabet=68	i=3		3<=3	j=0		0<=0	D	j=0+1=1		1<=3	D	j=1+1=2		2<=3	D	j=2+1=3	3<=3	D