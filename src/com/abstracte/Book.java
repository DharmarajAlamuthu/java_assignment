package com.abstracte;

public abstract class Book {
	
	abstract void write();
	abstract void read();

}
