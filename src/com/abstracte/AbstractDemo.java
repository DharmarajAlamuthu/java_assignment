package com.abstracte;

public class AbstractDemo extends Book {

	@Override
	void write() {
		System.out.println("Writing a Book ");
	}

	@Override
	void read() {
		System.out.println("Reading a Book");
	}
	
	public static void main(String[] args) {
		
		AbstractDemo book=new AbstractDemo();
		book.read();
		book.write();

	}

}
