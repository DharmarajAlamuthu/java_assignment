package com.abstracte;

public class NoteBook extends Book{

	@Override
	void write() {
		System.out.println("Writing a Book ");
	}

	@Override
	void read() {
		System.out.println("Reading a Book ");
	}
	
	void draw() {
	System.out.println("Draw the Book");	
	}
	
	public static void main(String[] args) {
		NoteBook book=new NoteBook();
		book.draw();
		book.read();
		book.write();
	}

}
