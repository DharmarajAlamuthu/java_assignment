package com.inheritance;

public class Child extends Parent{
	
	
	public void property2() {
		System.out.println("Child Property2");
	}

	public static void main(String[] args) {
		Parent p1=new Parent();
		p1.property1();
		
		System.out.println();
		
		Child c1=new Child();
		c1.property1();
		c1.property2();
		

	}

}
