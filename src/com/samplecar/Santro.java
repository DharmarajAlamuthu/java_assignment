package com.samplecar;

public class Santro extends Car implements BasicCar{
	
	void remoteStart() {
		System.out.println("Automatic change to remotestart");
	}

	@Override
	public void gearChange() {
		System.out.println("GeerChage 1 to 2 in the car");
	}

	@Override
	public void music() {
		System.out.println("Play music to car");
	}
	
	public static void main(String[] args) {
		Santro car=new Santro();
		car.drive();
		car.gearChange();
		car.music();
		car.remoteStart();
		car.disconnected();
		car.lift();
		car.stop();
	}

}
